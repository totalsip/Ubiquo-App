package la.applica.ubiquo.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import la.applica.ubiquo.Model.Notificacion;
import la.applica.ubiquo.R;

/**
 * Created by adrianayala on 22/05/15.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>
        implements View.OnClickListener{

    private View.OnClickListener listener;
    private Context context;

    private ArrayList<Notificacion> items;
    private int itemLayout;

    public RecyclerAdapter(Context context, ArrayList<Notificacion> items, int itemLayout){
        this.items = items;
        this.context = context;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView avatar;
        private TextView titulo;
        private TextView cuerpo;
        private TextView remite;
        private TextView fecha;

        public ViewHolder(View itemView) {
            super(itemView);

            avatar = (ImageView) itemView.findViewById(R.id.img_card_avatar);
            titulo = (TextView) itemView.findViewById(R.id.tv_titulo);
            cuerpo = (TextView) itemView.findViewById(R.id.tv_cuerpo);
            remite = (TextView) itemView.findViewById(R.id.tv_remite);
            fecha = (TextView) itemView.findViewById(R.id.tv_date);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Notificacion item = items.get(position);

        if (item.getEstado().equalsIgnoreCase("1")){
            holder.titulo.setTypeface(Typeface.SANS_SERIF);
            holder.titulo.setText(item.getTitulo());
        }else{
            holder.titulo.setText(item.getTitulo());
        }
        holder.cuerpo.setText(item.getCuerpo());
        holder.remite.setText(item.getRemite());
        holder.fecha.setText(item.getFecha());
        Picasso.with(context)  // Utilizamos Picasso para cargar el avatar en el cardview
                .load(item.getAvatar())
                .placeholder(R.drawable.account_circle_48)   // optional
                .error(R.drawable.account_circle_48)      // optional
                .resize(100, 100)                        // optional
                .rotate(0)                             // optional
                .into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);
    }

}
